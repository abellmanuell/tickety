import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./Home";
import Order from "./features/Order";
import Fans from "./features/Fans";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/order" element={<Order />} />
        <Route path="/fans" element={<Fans />} />
        <Route path="*" element={<h1>Not Page</h1>} />
      </Routes>
    </Router>
  );
}

export default App;
