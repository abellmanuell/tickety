import React from "react";
import InputField from "./utils/InputField";

function Order() {
  return (
    <section>
      <div className="from-bg-from-color to-bg-to-color bg-gradient-to-br text-white p-20 text-center">
        <h1 className="text-5xl">Meet and Greet.</h1>
        <p>Let's Make Memories Together!</p>
      </div>

      <section className="p-10">
        <form
          method="POST"
          action="/api/order_ticket"
          encType="multipart/form-data"
          className="p-10 md:w-[31.25em] mx-auto  space-y-8"
          autoComplete="on"
        >
          <InputField
            fieldname="Surname"
            type="text"
            name="surName"
            className="bg-gray-50 w-full p-4 rounded outline-none peer placeholder:text-transparent"
          />
          <InputField
            fieldname="Other Name"
            name="otherName"
            type="text"
            className="bg-gray-50 w-full p-4 rounded outline-none peer placeholder:text-transparent"
          />
          <InputField
            fieldname="Phone Number"
            name="phoneNumber"
            type="number"
            className="bg-gray-50 w-full p-4 rounded outline-none peer placeholder:text-transparent"
          />
          <InputField
            fieldname="Country"
            name="countryCity"
            type="text"
            className="bg-gray-50 w-full p-4 rounded outline-none peer placeholder:text-transparent"
          />
          <InputField
            fieldname="Email Address"
            name="email"
            type="email"
            className="bg-gray-50 w-full p-4 rounded outline-none peer placeholder:text-transparent"
          />
          <InputField
            fieldname="Date of Birth"
            name="dateOfBirth"
            type="date"
            className="bg-gray-50 w-full p-4 rounded outline-none peer placeholder:text-transparent"
          />
          <InputField
            fieldname="Identification Card Number"
            name="identificationNumber"
            type="number"
            className="bg-gray-50 w-full p-4 rounded outline-none peer placeholder:text-transparent"
          />
          <InputField
            fieldname="Identification Card Photo"
            name="identificationCard"
            type="file"
            className="w-full file:rounded-full file:border-none file:py-2 file:px-6 file:cursor-pointer"
          />
          <InputField
            fieldname="Reasons for meeting..."
            name="meetPurpose"
            type="text"
            className="bg-gray-50 w-full p-4 rounded outline-none peer placeholder:text-transparent"
          />
          <input
            type="submit"
            value="Submit"
            className="from-bg-from-color to-bg-to-color bg-gradient-to-br w-full p-6 text-white text-2xl cursor-pointer rounded"
          />
        </form>
      </section>
    </section>
  );
}

export default Order;
