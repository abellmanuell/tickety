import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

async function getData(url) {
  try {
    const response = await axios.get(url);
    return await response.data;
  } catch (err) {
    console.error(err);
  }
}

function Fans() {
  const [fans, setFans] = useState([]);
  useEffect(() => {
    (async () => {
      const result = await getData(
        "https://tickety-d1q0.onrender.com/api/order_list"
      );
      setFans(result);
    })();
  }, []);

  const listFans = fans.map((fan) => {
    return <FanProfile {...fan} />;
  });
  return (
    <div>
      <table className="w-full">
        <thead className="text-left border">
          <th>Surname</th>
          <th>Other Name</th>
          <th>Phone Number</th>
          <th>ID NO</th>
        </thead>
        {listFans}
      </table>
    </div>
  );
}

function FanProfile({
  surName,
  otherName,
  phoneNumber,
  identificationNumber,
  avatar_url,
}) {
  return (
    <tbody className="text-left border">
      <td>{surName}</td>
      <td>{otherName}</td>
      <td>{phoneNumber}</td>
      <td>{identificationNumber}</td>
      <td>
        <Link to={`https://tickety-d1q0.onrender.com/${avatar_url}`}>
          <img
            src={`https://tickety-d1q0.onrender.com/${avatar_url}`}
            alt={surName + " " + otherName}
            width={70}
            height={70}
          />
        </Link>
      </td>
    </tbody>
  );
}

export default Fans;
