import React, { forwardRef } from "react";

const InputField = forwardRef(function InputField(props, ref) {
  return (
    <div className="relative">
      <input
        type={props.type}
        name={props.fieldname.toLowerCase()}
        id={props.fieldname.toLowerCase()}
        {...props}
        placeholder={props.fieldname}
        required
      />
      <label
        htmlFor={props.fieldname.toLowerCase()}
        className="absolute -top-6 left-0 text-gray-600 text-sm peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-400 peer-placeholder-shown:top-[1.15rem] peer-placeholder-shown:left-4 transition-all"
      >
        {props.fieldname}
      </label>
    </div>
  );
});

export default InputField;
