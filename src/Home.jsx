import React, { useState } from "react";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function Home() {
  const settings = {
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 2000,
    cssEase: "linear",
    lazyLoad: "ondemand",
  };
  return (
    <React.Fragment>
      <section className="min-h-screen bg-top bg-contain bg-no-repeat from-bg-from-color to-bg-to-color bg-gradient-to-br uppercase p-6 md:py-10  flex flex-col md:flex-row-reverse justify-center space-y-10 md:space-y-0">
        <article>
          <h1 className="text-5xl drop-shadow-lg text-white">Hurry Up</h1>
          <h2 className="text-4xl drop-shadow-lg text-white">
            to order concert tickets!
          </h2>
        </article>

        <article className="self-center">
          <img src="/hurryup.svg" width="550px" loading="lazy" alt="Hurry Up" />
        </article>

        <article className="md:content-start md:self-center flex flex-col-reverse md:flex-col ">
          <div className="my-10">
            <p className="text-2xl text-chestnut-rose-200">
              In the last row...
            </p>

            <div className="h-24 md:w-80 m-5">
              <Slider {...settings}>
                <div className="h-24">
                  <img src="/apm.png" alt="APM" />
                </div>
                <div className="h-24 ">
                  <img src="/budweiser.png" alt="Budweiser" />
                </div>
                <div className="">
                  <img src="/youtubemusic.png" alt="Youtube Music" />
                </div>
                <div className="h-24 ">
                  <img src="/applemusic.png" alt="Apple Music" />
                </div>
                <div className="h-24 ">
                  <img src="/transparentDTMlogo.png" alt="Transparent" />
                </div>
              </Slider>
            </div>
          </div>

          <div className="text-center md:text-left">
            <Link
              to="/order"
              className="text-3xl text-white bg-wild-watermelon-500 py-2 px-6 rounded-lg shadow-lg animate-bounce inline-block hover:animate-none transition-all"
            >
              Order
            </Link>
          </div>
        </article>
      </section>
    </React.Fragment>
  );
}

export default Home;
