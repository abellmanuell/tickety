import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";

const baseURL = "https://tickety-d1q0.onrender.com";
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    proxy: {
      "^/api": "https://tickety-d1q0.onrender.com",
    },
  },
});
