/** @type {import('tailwindcss').Config} */
const plugin = require("tailwindcss/plugin");

export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
    "node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}",
  ],

  theme: {
    fontFamily: {
      sans: ["Bebas Neue", "sans-serif"],
    },

    extend: {
      colors: {
        "bg-from-color": "#e85c6e",
        "bg-to-color": "#6c1925",
        "chestnut-rose": {
          DEFAULT: "#D25566",
          50: "#F8E6E9",
          100: "#F4D6DA",
          200: "#ECB6BD",
          300: "#E396A0",
          400: "#DB7583",
          500: "#D25566",
          600: "#BD3245",
          700: "#912635",
          800: "#641B25",
          900: "#380F14",
          950: "#22090C",
        },

        "wild-watermelon": {
          DEFAULT: "#FF677E",
          50: "#FFFFFF",
          100: "#FFFFFF",
          200: "#FFE1E6",
          300: "#FFB9C3",
          400: "#FF90A1",
          500: "#FF677E",
          600: "#FF2F4E",
          700: "#F60025",
          800: "#BE001D",
          900: "#860014",
          950: "#6A0010",
        },
      },
    },
  },
  plugins: [],
};
